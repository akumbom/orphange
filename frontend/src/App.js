import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Child from './components/Child';
import Home from './components/Home';
import About from './components/About';
// import logo from './logo.svg';
import './App.css';
import t_slider_13 from '../public/img/t_slider_13.jpg';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom'

export default class App extends Component {

  render(){

    return(
      <div className='App'>
       <div className="jumbotron container-fluid text-white" style={{backgroundImage: 'url('+t_slider_13 + ')',height: '100px' }}>
          <h1 className="marquee"> WELCOME TO OUR ORHPANAGE</h1>
          <hr className="my-4" />
          {/* <p className="lead marquee">
            <strong>Welcome to the home of the underpriviledged. You can start by Sponsoring a child!!</strong>
          </p> */}
        </div>

        <nav className="navbar navbar-expand-lg bg-info">
          <a className="navbar-brand text-white" href="#">ORHPANAGE</a>
          <button className="navbar-toggler bg-white" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNavDropdown">
            <ul className="navbar-nav mx-auto">
              <Router className="nav-item active">
                <a  href="/" className="nav-link headers">Home</a>
              </Router>
              <Router className="nav-item">
                <a className="nav-link headers" href="/about">About Us</a>
              </Router>
              <li className="nav-item dropdown">
                <a className="nav-link dropdown-toggle headers" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Our Children
                </a>
                <div className="dropdown-menu dropdown-content" aria-labelledby="navbarDropdownMenuLink">
                  <Router className="nav-item active">
                    <a href="/children" className="dropdown-item  dropdown-btn">All</a>
                  </Router>
                  <Router className="nav-item active">
                    <a  href="/children" className="dropdown-item  dropdown-btn">Girls</a>
                  </Router>
                  <Router className="nav-item active">
                    <a  href="/children" className="dropdown-item  dropdown-btn">Boys</a>
                  </Router>
                </div>
              </li>
              <li className="nav-item">
                <a className="nav-link headers" href="#">Our Sponsors</a>
              </li>
              <li className="nav-item">
                <a className="nav-link headers" href="#">Our Employees</a>
              </li>
              <li className="nav-item">
                <a className="nav-link headers" href="#">Our Projects</a>
              </li>
            </ul>
            <ul className="navbar-nav ml-auto">
                <li className="nav-item">
                    <a className="nav-link text-white" href="#" role="button"><button className="pulse-button text-white grow volunteer">Volunteer</button></a>
                </li>
                <li className="nav-item">
                    <a className="nav-link text-white" href="#" role="button"><button className="pulse-button text-white grow donate">Donate</button></a>
                </li>
            </ul>
          </div>
        </nav>
        

      <div className="container-fluid body">
                        
          {/* Route section */}
          <div className="row body-content">
              {/* <div className="col-sm-2"> 
                  <ul className="sidebar-nav">
                    <li><a href="{% url 'index' %}">Home</a></li>
                    <li><a href="">All Children</a></li>
                    <li><a href="">All Sponsors</a></li>
                  </ul>
              </div>  */}

          </div>
          <Router>
            <Route exact path="/" component={Home}/>
          </Router>
          <Router>
            <Route path="/children" component={Child}/>
          </Router>
          <Router>
            <Route path="/about" component={About}/>
          </Router>


        </div>

      </div>
    );
  }
}

if(document.getElementById('root')){
  ReactDOM.render(
    <App />,
    document.getElementById('root')
  );
}

