import React, {  Component} from 'react';
import ReactDOM from 'react-dom';
import t_slider_13 from '../../public/img/t_slider_13.jpg';

import {
    BrowserRouter as Router,
    Route,
    Link,
    Switch,
    Redirect
  } from 'react-router-dom'

export default class Child extends Component{
    
    constructor(props){
        super(props);
        this.state = {
            children : [],
            baseApi : 'http://localhost:8000/api/children/',
        }
    }
 
    async componentDidMount(){
        try{
            const res = await fetch(this.state.baseApi);
            const children = await res.json();
            this.setState({
                children
            });
        }catch(e){
            console.log(e);
        }
    }

    getChildKey(child){
       let key = child.childId + 1;
       return child.firstName + key.toString();
    }
    
    async filterByGirls(){
        try {
            const URL = this.state.baseApi + 'girls/';
            const res = await fetch(URL);
            const girls = await res.json();
            this.setState({
                girls
            });
        }catch(e){
            console.log(e);
        }
    }
    async filterByBoys(){
        try {
            const URL = this.state.baseApi + 'boys/';
            const res = await fetch(URL);
            const boys = await res.json();
            this.setState({
                boys
            });
        }catch(e){
            console.log(e);
        }
    }

    getSponsorToolTipMessage(child){
        let msg = 'Sponsor ' + child.firstName + ' ' + child.lastName;
        return msg;
    }

    render(){
        return(
            <div className="container-fluid text-center row jumbotron m-2">
              {
                  this.state.children.map(child => (
                    <div className="card m-2 bg-info col-sm-2 text-light border-dark border-1" key={this.getChildKey(child)}>
                        <img className="card-img-top img-responsive" src={t_slider_13} alt="Card image cap" height="150px"/>
                        <div className="card-body bg-dark">
                        <h5 className="card-title">{ child.firstName} {child.lastName} ({child.sex})</h5>
                        <p className="card-text well text-dark" style={{background:'#fff'}}>{child.details}</p><br/>
                        <a href="#"><button className="btn donate btn-sm btn-warning pull-center pulse-button grow text-light" data-toggle="tooltip" data-placement="bottom" title={this.getSponsorToolTipMessage(child)}>Sponsor </button></a>
                        </div>
                     </div>
                  ))
              }
              <br/><br/>
            </div>
        );

    }
  
}

<Route path='/children/girls/'  render = {
    () => {return(
        this.state.girls.map(child => (
            <div className="card m-2 bg-info col-sm-2 text-light border-dark border-1" key={this.getChildKey(child)}>
                <img className="card-img-top img-responsive" src={t_slider_13} alt="Card image cap" height="150px"/>
                <div className="card-body bg-dark">
                <h5 className="card-title">{ child.firstName} {child.lastName} ({child.sex})</h5>
                <p className="card-text well text-dark" style={{background:'#fff'}}>{child.details}</p><br/>
                <a href="#"><button className="btn donate btn-sm btn-warning pull-center pulse-button grow text-light" data-toggle="tooltip" data-placement="bottom" title={this.getSponsorToolTipMessage(child)}>Sponsor </button></a>
                </div>
             </div>
          ))
    )}} />


if(document.getElementById('child')){
    ReactDOM.render(
      <Child />,
      document.getElementById('child')
    );
  }