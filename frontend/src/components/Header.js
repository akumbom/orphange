import React, { Component } from 'react';

export default class Header extends Component {

  render() {
    return (
    <nav className="navbar navbar-expand-lg bg-info">
      <a className="navbar-brand text-white" href="#">ORHPANAGE</a>
      <button className="navbar-toggler bg-white" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNavDropdown">
        <ul className="navbar-nav mx-auto">
          <li className="nav-item active">
            <a className="nav-link headers" href="">Home <span className="sr-only">(current)</span></a>
          </li>
          <li className="nav-item">
            <a className="nav-link headers" href="#">About Us</a>
          </li>
          <li className="nav-item dropdown">
            <a className="nav-link dropdown-toggle headers" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Our Children
            </a>
            <div className="dropdown-menu dropdown-content" aria-labelledby="navbarDropdownMenuLink">
              <a className="dropdown-item dropdown-btn" href="#">Boys</a>
              <a className="dropdown-item  dropdown-btn" href="#">Girls</a>
              <a className="dropdown-item  dropdown-btn" href="#">All</a>
            </div>
          </li>
          <li className="nav-item">
            <a className="nav-link headers" href="#">Our Sponsors</a>
          </li>
          <li className="nav-item">
            <a className="nav-link headers" href="#">Our Employees</a>
          </li>
          <li className="nav-item">
            <a className="nav-link headers" href="#">Our Projects</a>
          </li>
        </ul>
        <ul className="navbar-nav ml-auto">
            <li className="nav-item">
                <a className="nav-link text-white" href="#" role="button"><button className="pulse-button text-white grow volunteer">Volunteer</button></a>
            </li>
            <li className="nav-item">
                <a className="nav-link text-white" href="#" role="button"><button className="pulse-button text-white grow donate">Donate</button></a>
            </li>
        </ul>
      </div>
    </nav>
    )}
}

