import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import './index.css';

if(document.getElementById('root')){
  ReactDOM.render(
    <App />,
    document.getElementById('root')
  );
}

// if(document.getElementById('home')){
//   ReactDOM.render(
//     <Home />,
//     document.getElementById('home')
//   );
// }
