from django.contrib import admin

# Register your models here.
from .models import Child, Sponsor

@admin.register(Child)
class ChildAdmin(admin.ModelAdmin):
    list_display = ('firstName','lastName','sex','dob','details')

@admin.register(Sponsor)
class SponsorAdmin(admin.ModelAdmin):
    list_display = ('firstName','lastName','sex','details')
