from django.db import models
from django.utils import timezone

# Create your models here.

class Child(models.Model):
    """
    Typical class defining a Child Object
    """
    #myfields
    childId = models.AutoField(primary_key = True)
    firstName = models.CharField(max_length = 100)
    lastName = models.CharField(max_length=100)
    FEMALE = 'F'
    MALE = 'M'
    OTHER = 'O'
    SEX_CHOICES = (
        (FEMALE,'Female'),
        (MALE,'Male'),
        (OTHER,'Other')
    )
    sex = models.CharField(max_length=10,choices=SEX_CHOICES,default='Select')
    dob = models.DateField(auto_now=False,blank=True, null=True)
    details = models.TextField(max_length=2000,blank=True, null=True,help_text='Enter some details about child')
    is_sponsored = models.BooleanField(default=False)
    created_on = models.DateField(auto_now=False,auto_now_add=False,default=timezone.now)
    updated_on = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True)

    class Meta:
        ordering =['firstName','lastName']

    def get_absolute_url(self):
        return reversed('children',args=[str(self.childId)])

    def __str__(self):
        fullName = str(self.firstName) + ' ' + str(self.lastName)
        return fullName


class Sponsor(models.Model):
    """
    Typical class defining a SPonsor Object
    """
    #myfields
    sponsorId = models.AutoField(primary_key = True)
    firstName = models.CharField(max_length = 100)
    lastName = models.CharField(max_length=100)
    FEMALE = 'F'
    MALE = 'M'
    OTHER = 'O'
    SEX_CHOICES = (
        (FEMALE,'Female'),
        (MALE,'Male'),
        (OTHER,'Other')
    )
    sex = models.CharField(max_length=10,choices=SEX_CHOICES,default='Select')
    details = models.TextField(max_length=2000,blank=True, null=True,help_text='Enter some details about child')
    created_on = models.DateField(auto_now=False,auto_now_add=False,default=timezone.now)
    updated_on = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True)

    class Meta:
        ordering =['firstName','lastName']

    def get_absolute_url(self):
        return reversed('children',args=[str(self.sponsorId)])

    def __str__(self):
        fullName = str(self.firstName) + ' ' + str(self.lastName)
        return fullName

    

    





