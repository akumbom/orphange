from rest_framework import serializers
from .models import Child, Sponsor

class ChildSerializer(serializers.ModelSerializer):
    """
    Child serializer
    """
    class Meta: 
        fields = (
            'firstName',
            'lastName',
            'sex',
            'details',
            'created_on',
            'updated_on',
            'dob',
            'is_sponsored'
        )
        model = Child


class SponsorSerializer(serializers.ModelSerializer):
    """
    Sponsor serializer
    """
    class Meta:
        fields = (
            'firstName',
            'lastName',
            'sex',
            'details'
        )
        model = Sponsor
