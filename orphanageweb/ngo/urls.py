from django.urls import path
from . import views

# Children url
urlpatterns = [
    path('children/', views.ListChildrenView.as_view()),
    path('children/boys/',views.ListBoysFromChildren.as_view()),
    path('children/girls/',views.ListGirlsFromChildren.as_view()),
    path('children/<int:pk>/',views.DetailChildView.as_view()),
]

#Sponsor url
urlpatterns += [
    path('sponsors/',views.ListSponsorsView.as_view()),
    path('sponsors/<int:pk>/',views.DetailSponsorView.as_view()),
]