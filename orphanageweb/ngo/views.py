from django.shortcuts import render
from rest_framework import generics
from ngo.serializers import ChildSerializer, SponsorSerializer
from ngo.models import Child, Sponsor

# Children View
class ListChildrenView(generics.ListAPIView):
    '''
    Typical Children list view
    '''
    serializer_class = ChildSerializer
    queryset = Child.objects.all()

class ListBoysFromChildren(generics.ListAPIView):
    '''
    Filter by boys
    '''
    serializer_class = ChildSerializer
    queryset = Child.objects.filter(sex='M')
class ListGirlsFromChildren(generics.ListAPIView):
    '''
    Filter by girls
    '''
    queryset = Child.objects.filter(sex='F')
    serializer_class = ChildSerializer

class DetailChildView(generics.RetrieveUpdateDestroyAPIView):
    '''
    Detail Child View
    '''
    queryset = Child.objects.all()
    serializer_class = ChildSerializer


class ListSponsorsView(generics.ListAPIView):
    '''
    Typical Sponsors list view
    '''
    queryset = Sponsor.objects.all()
    serializer_class = SponsorSerializer

class DetailSponsorView(generics.RetrieveUpdateDestroyAPIView):
    '''
    Detail Sponsor view
    '''
    queryset = Sponsor.objects.all()
    serializer_class = SponsorSerializer
